#!/bin/bash

# scratch folder
scratch_folder="/scratch"

# pptx2txt.sh script location
pptx2txt="/root/pptx2txt.sh-0.1.0/pptx2txt.sh"

# loop over list of input filenames
for fullfile in "$@"; do

  filename=$(basename "$fullfile")
  dirname=$(dirname "$fullfile")
  extension="${filename##*.}"
  fileprefix="${filename%.*}"

  # convert PPT to PPTX if necessary
  if [[ $extension =~ [pP][pP][tT]$ ]]; then
    libreoffice --headless --convert-to pptx "$scratch_folder/$dirname/$fileprefix.$extension" --outdir "$scratch_folder/$dirname"
    extension="pptx"
  fi

  # convert KEYNOTE to PPTX if necessary
  if [[ $extension =~ [kK][eE][yY]$ ]]; then
    libreoffice --headless --convert-to pptx "$scratch_folder/$dirname/$fileprefix.$extension" --outdir "$scratch_folder/$dirname"
    extension="pptx"
  fi

  # convert PowerPoint PPTX to text
  if [[ $extension =~ [pP][pP][tT][xX]$ ]]; then
    $pptx2txt --verbose "$scratch_folder/$dirname/$fileprefix.$extension" > "$scratch_folder/$dirname/$fileprefix.txt"
  else
    echo "unknown extension $extension"
  fi

done
